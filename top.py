#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
AI09 A21 Team Orienteering Problem
Implementation of an elastic net heuristic to solve a TOP instance
@Authors:
    RAMOS IRETA José Santiago
    SOLAN Matthieu
"""

import random, sys, os
from math import sqrt, acos
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import networkx as nx
from scipy.spatial.distance import pdist, squareform

#Initialization data for random instance generation
initDict = {
    'n_customers':20, #number of customers / nodes
    'n_trucks':3,   #number of trucks
    'd_truck': 10.,  #maximum distance for a truck
    'surface':100 #only used as a boundary for x,y,q node generator
    }

class TOP():
    def __init__(self, filepath=None):
        #Get instance values for n_ customers, n_trucks, d_trucks, and customers_x,_y,_q
        if filepath==None:
            self.generateInstance(initDict)
        else:
            self.loadInstance(filepath)
        #transpose x y arrays into an array of coordinates
        self.coords = np.array([self.customers_x, self.customers_y]).T
        #Calculate Distance Matrix
        self.DistanceMat = np.array(squareform(pdist(self.coords)))
        #self.DistanceMat = np.sqrt(np.sum((self.coords[None, :] - self.coords[:, None])**2, -1))
        #Initialize control variables
        self.remainingDistances = [self.d_truck] * self.n_trucks
        #node 0 is considered as starting point for every truck tour
        self.remainingNodes = range(1, self.n_customers)
        self.printInstanceData()

    def generateInstance(self, values):
        """Generate a Random instance with default init values

        Args:
            values (Dict): Dictionary of default values for the instance
        """
        #generate coordinates and profit values for customer nodes using surface value as a limit
        customers = [random.sample(range(values['surface']), 3) for x in range(values['n_customers'])]
        #initialize instance default variables
        self.n_customers = values['n_customers']
        self.n_trucks = values['n_trucks']
        self.d_truck = values['d_truck']
        #convert generated list to individual np arrays
        self.customers_x = np.array([customers[x][0] for x in range(self.n_customers)], dtype=np.float32)
        self.customers_y = np.array([customers[y][1] for y in range(self.n_customers)], dtype=np.float32)
        self.customers_q = np.array([customers[q][2] for q in range(self.n_customers)], dtype=np.int16)
        #write generated instance data to a file
        self.dumpInstanceData(customers)

    def loadInstance(self, filepath):
        """Load Instance values from a datafile

        Args:
            filepath (str): name and path of the file (just name if running within the same folder)
        """
        parameters = pd.read_csv(filepath, sep=" ", header=None, nrows=3)
        self.n_customers = int(parameters.iloc[0,1])
        self.n_trucks = int(parameters.iloc[1,1])
        self.d_truck = float(parameters.iloc[2,1])
        values = pd.read_csv(filepath, sep=",", header=None, names=("x","y", "q"), skiprows=(0,1,2))
        self.customers_x = np.array(values.iloc[:,0], dtype=np.float32)
        self.customers_y = np.array(values.iloc[:,1], dtype=np.float32)
        self.customers_q = np.array(values.iloc[:,2], dtype=np.int16)

    def dumpInstanceData(self, customers):
        """write generated instance data to file

        Args:
            customers (List): List of 3 value tuples containing x,y,q data for each customer/node
        """
        dir = os.path.dirname(os.path.abspath(__file__))
        print("writing instance data to file: ", dir+'/randomInstance.txt')
        with open('randomInstance.txt', 'w') as f:
            f.write(f'n_customers {self.n_customers}\n')
            f.write(f'n_trucks {self.n_trucks}\n')
            f.write(f'd_truck {self.d_truck}\n')
            for node in customers:
                f.write(f'{node[0]},{node[1]},{node[2]}\n')

    def printInstanceData(self):
        print('total customers:{}'.format(self.n_customers))
        print('number of trucks:{}\tdistance:{}'.format(self.n_trucks, self.d_truck))
        self.printValues()

    def printValues(self):
        """Debugging print for array and matrix control"""
        print("*"*80)
        print("customer coordinates:\n", self.coords)
        print("customer profits:\n", self.customers_q)
        print("Distance Matrix:\n", self.DistanceMat)
        print("*"*80)

    def getValidNodeDistances(self, node, k):
        """calculate the index of valid nodes for a given position(node) and a given truck

        Args:
            node (int): index of the current node
            k (int): index of the current truck

        Returns:
            np.array: numpy array of distances to the indexed nodes.
        """
        #dist = np.array([np.linalg.norm(self.coords[node] - self.coords[i]) for i in self.remainingNodes])
        distances = self.DistanceMat[node, :]
        retDistances = self.DistanceMat[:, 0]
        totalDistance = np.array(distances+retDistances)
        #TODO: starting point problematic here?
        nodeIndex = np.nonzero(totalDistance<=self.remainingDistances[k])
        print(f'remaining distance for truck{k}: {self.remainingDistances[k]}')
        print(f'distances for node{node}:', distances)
        print(f'return distances:', retDistances)
        print(f'total distances for node{node}:', totalDistance)
        print('validNodes: ',nodeIndex)
        return nodeIndex

    def sortCoord(self):
        L = self.angleList()
        for i in range(2,self.n_customers):
            angleMax = 0
            coord = 0
            for y in range(i+1,self.n_customers):
                if(L[y][3] > angleMax):
                    angleMax = L[y][3]
                    coord = y

            nodeSave = L[i]
            L[i] = L[y]
            L[y] = nodeSave


        return L

    def angleList(self):
        List = [[self.customers_x[0],self.customers_y[0],0,0]]
        List = List + [[self.customers_x[1],self.customers_y[1],self.customers_q[1],0]]
        for i in range(2,self.n_customers):
            angle = self.calculatedAngle(1,i)
            List = List + [[self.customers_x[i], self.customers_y[i], self.customers_q[i], angle]]

        return List

    def calculatedAngle(self,i,y):
        xi = self.customers_x[i] - self.customers_x[0]
        xy = self.customers_x[y] - self.customers_x[0]
        yi = self.customers_y[i] - self.customers_y[0]
        yy = self.customers_y[y] - self.customers_y[0]
        di = np.linalg.norm(self.coords[0] - self.coords[i])
        dy = np.linalg.norm(self.coords[0] - self.coords[y])
        angle = acos((xi*xy + yi*yy)/(di*dy))
        return angle

        """
        def calculatedAngle(self,x1,y1,x2,y2,x3,y3):
        xi = x2 - x1
        xy = x3 - x1
        yi = y2 - y1
        yy = y3 - y1
        di = np.linalg.norm([x1,y1] - [x2,y2])
        dy = np.linalg.norm([x1,y1] - [x3,y3])
        angle = acos((xi*xy + yi*yy)/(di*dy))
        return angle
        """
    def region(self):
        L = sortCoord()
        range (L[-1][3] - L[1][3])
        for i in range(self.n_trucks):
            angle = 0;
            #while (angle < range) & & (len(self.coords) > self.n_customers):


    def getNextNeighbor(self, truck, node):
        """Return next node to be added to a given truck tour

        Args:
            truck (int): index of the current truck
            node (int): index of the current node

        Returns:
            int: index of the chosen node
        """
        #TODO interestlist is ok?
        validNodes = self.getValidNodeDistances(node, truck)
        if validNodes == 0:
            raise(f'No valid neighbor for node{node}')
        interestList = np.array(self.DistanceMat[node,:]/self.customers_q)
        candidateNode = np.argmax(interestList)
        while(not np.any(validNodes == candidateNode)):
            if validNodes == 0:
                raise(f'No valid neighbor for node{node}')
            interestList[candidateNode] = 0
            candidateNode = np.argmax(interestList)
        print(f'currentNode:{node}\tnextNode:{candidateNode}')
        print(interestList)
        return candidateNode

    def getNearestNeighbor(self, node):
        """Return nearest neighbor from remaining nodes for a given node

        Args:
            node (int): index of the current node
            truck(int): index of the current truck

        Returns:
            int: index of the chosen node
        """
        temp = np.array(self.DistanceMat[node,:])
        #distance to itself is 0 therefore it has to be infinite to find a correct node
        temp[node] = 10000
        candidateNode = np.argmin(temp)
        while(not candidateNode in self.remainingNodes):
            #lock the candidateNode to search a new one in the array
            temp[candidateNode] = 10000
            candidateNode = np.argmin(temp)
        print(f'currentNode:{node}\tnextNode:{candidateNode}')
        return candidateNode

    def GNAlgorithm(self):
        #TODO: finish and make corrections
        for k in self.n_trucks:
            flag = True
            order = []
            #starting point
            currentNode = 0
            while flag:
                try:
                    nextNode = self.getNextNeighbor(k, currentNode)
                    self.remainingDistances[k] = self.remainingDistances[k] - self.DistanceMat[currentNode,nextNode]
                    currentNode = nextNode
                    self.remainingNodes.remove(nextNode)
                except:
                    flag = False

    def run(self):
        #TODO
        print("TODO run")

    def drawGraph(self):
        """plot the solution's graph using networkx library"""
        #nx.draw option dictionary passed as kwargs for attributes
        options = {
            'node_color': 'yellow',
            'node_size': self.customers_q,
            }
        #colorbox list for plot legends
        colorbox = []
        #Generating random list of k colors for each truck
        colors = []
        for i in range(self.n_trucks):
            colors.append('#%06X' % random.randint(0, 0xFFFFFF))
        #Graph initialization
        G = nx.DiGraph()
        G.add_nodes_from(range(self.n_customers))
        #networkx Graph plot requires a dictionary with node index as key and coordinates tuple as value
        nodes = { i : (self.coords[i]) for i in range(self.n_customers)}
        nx.draw_networkx_nodes(G, pos=nodes, **options)
        nx.draw_networkx_labels(G, pos=nodes)
        #Drawing edges for each truck's found path
        for k in range(self.n_trucks):
            #TODO add edges and plot them according to each truck calculated tour
            #random sample to test graph visual
            order = random.sample(self.remainingNodes, len(self.remainingNodes)//(self.n_trucks-k))
            self.remainingNodes = [node for node in self.remainingNodes if node not in order]
            #networkx edgelist must be a list of tuples with the tuples being the connected nodes index
            ktruckedges = [(order[i],order[i+1]) for i in range(len(order)-1)]
            #adding the first and last edge to complete the return to warehouse
            ktruckedges.append((0, order[0]))
            ktruckedges.append((order[-1], 0))
            print(f"truck{k} color:{colors[k]} path:", order)
            nx.draw_networkx_edges(G, pos=nodes, edgelist=ktruckedges, edge_color=colors[k], label=f'truck{k}')
            #plot legend handling
            colorbox.append(mpatches.Patch(color=colors[k], label=f'Truck{k}'))
        plt.legend(handles=colorbox, loc='best')
        plt.show()

if len(sys.argv)<2:
    print("Evaluating Random Instance")
    t = TOP()
else:
    print("Evaluating instance in file: {}".format(sys.argv[1]))
    t = TOP(sys.argv[1])
#t.drawGraph()
#t.getValidNodeDistances(0, 0)
#t.getNextNeighbor(0,0)
#tes = t.sortCoord()
#print(tes)
#for i in range(t.n_customers):
#    t.getNearestNeighbor(i)
l = t.sortCoord()
print(l)
